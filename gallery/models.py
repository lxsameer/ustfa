from django.db import models
from django.contrib.auth.models import User
from django.utils.translation import ugettext as _
from django.contrib.sites.models import Site
from django.conf import settings


class Gallery(models.Model):
    """
    Page main model class
    """
    title = models.CharField(max_length=30,
                             verbose_name=_("Title"))
    slug = models.SlugField(max_length=30, unique=True,
                            verbose_name=_("Slug"))
    date = models.DateTimeField(auto_now_add=True, auto_now=False,
                                     verbose_name=_('Date and Time'))

    def __unicode__(self):
        return self.title

    @models.permalink
    def get_absolute_url(self):
        return ("gallery.views.show_gallery" % self.slug)

    class Meta:
        verbose_name_plural = _("Galleries")
        verbose_name = _('Gallery')
        ordering = ['-date']
