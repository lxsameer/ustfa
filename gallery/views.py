import os

from django.shortcuts import render_to_response as rr
from django.http import Http404
from django.conf import settings

from gallery.models import Gallery


def show_gallery(request, gallery):
    try:
        gallery = Gallery.objects.get(slug=gallery)
    except Gallery.DoesNotExist:
        raise Http404()

    path = os.path.join(settings.ROOT,
                        '../statics/galleries/%s/' % gallery.slug)
    files = os.listdir(path)

    if not files:
        raise Http404("No file found.")

    images = []
    for file_ in files:

        dirname = os.path.dirname(os.path.join(path, file_))
        filename = os.path.basename(file_)

        if filename[-4:].lower() in [".png", ".jpg"]:
            thum = filename
            images.append(["/statics/galleries/%s/%s" %  (gallery.slug, file_),
                           "/statics/galleries/%s/thums/%s" % (gallery.slug, thum)])

    ffile = images[0][0]

    from pprint import pprint 
    pprint(images)

    return rr("gallery.html", {"images": images,
                               "ffile": ffile,
                               "album": gallery.title})


def index(request):
    galleries = Gallery.objects.all()
    return rr("gindex.html", {"galleries": galleries})
