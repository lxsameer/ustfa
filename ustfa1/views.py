from django.shortcuts import render_to_response as rr
from django.template import RequestContext


def index(request):
    return rr('index.html', {},
              context_instance=RequestContext(request))
