import os

from django.conf.urls import patterns, include, url
from django.conf import settings

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    url(r'^admin/', include(admin.site.urls)),
    (r'^pages/', include("page.urls")),
    (r'^news/', include("news.urls")),
    (r'^contact/', include("vanda.apps.contactus.urls")),

    (r'gallery/$', "gallery.views.index"),
    (r'gallery/([^/]+)/$', "gallery.views.show_gallery"),
    (r'^$', 'ustfa1.views.index'),
)


if settings.DEBUG:
    urlpatterns += patterns('',
            (r'^statics/(?P<path>.*)$', 'django.views.static.serve',
             {'document_root': os.path.join(os.path.dirname(__file__),
                                    '../statics/').replace('\\', '/')}),
)
