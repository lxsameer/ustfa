$(function(){
    $("#slides").slides({
        preload: true,
        play: 5000,
        pause: 2500,
        hoverPause: true,
	pagination: false,
	preloadImage: '/statics/images/loading.gif'
    });
    
    $("ul.sf-menu").superfish();
});
