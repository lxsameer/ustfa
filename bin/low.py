import os
import sys

import Image

files = os.listdir(sys.argv[1])
print ">>> ", files
try:
    os.mkdir(os.path.join(sys.argv[1], "low"))
except OSError:
    pass

for f in files:
    outfile = os.path.abspath(os.path.join(sys.argv[1], "low/%s" % f))
    print outfile
    if f != outfile and f[-4:].lower() == ".jpg":
        try:
            g = os.path.join(sys.argv[1], f)
            print ">G>> ", g
            im = Image.open(g)
            img_size = im.size
            size = 1024, 1024*img_size[1]/img_size[0]
            im.thumbnail(size, Image.ANTIALIAS)
            im.save(outfile, "JPEG")
        except IOError:
            print "cannot create thumbnail for '%s'" % f
            raise
